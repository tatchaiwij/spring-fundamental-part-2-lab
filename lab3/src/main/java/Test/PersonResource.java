package Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersonResource {
    
    @Autowired
    private Person person;

    public Person getPerson(){
        return this.person;
    }

    public void setPerson(Person personInput){
        this.person = personInput;
    }
}
