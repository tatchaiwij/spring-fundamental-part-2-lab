package Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    
    public static void main(String[] args){

        try(AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Appconfig.class)){
            Library library = context.getBean(Library.class);
            library.listBooks();
        }
    }
}
