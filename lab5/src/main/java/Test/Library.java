package Test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Library {
    
    @Autowired
    List<Book> books;

    public void listBooks(){
        for(Book book : books){
            System.out.println(book.getName());
        }
    }
}
