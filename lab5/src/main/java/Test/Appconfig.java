package Test;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("Test")
public class Appconfig {
    
    @Bean
    List<Book> books() {
        return Arrays.asList(new Book("Writing Code that Nobody Else Can Read 101"), new Book("Copying and Pasting from Stack Overflow 101"));
    }
}
