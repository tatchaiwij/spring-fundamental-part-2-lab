package Test;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("Test")
public class Appconfig {
    
    @Bean
    public Person person() {
        Person person = new Person();
        return person;
    }
}
