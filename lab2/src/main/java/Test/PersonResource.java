package Test;
import javax.annotation.Resource;

import org.springframework.stereotype.Component;

@Component("personresource")
public class PersonResource {
    
    @Resource(name = "person")
    private Person person;

    public Person getPerson(){
        return this.person;
    }

    public void setPerson(Person personInput){
        this.person = personInput;
    }
}
