package Test;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    
    public static void main(String[] args){

        try(AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Appconfig.class)){
            Person person = context.getBean(Person.class);
            person.setFullName("fullName");
            person.setAge(10);
            person.setGender("M");
            System.out.println(person.getFullName());
            System.out.println(person.getAge());
            System.out.println(person.getGender());
        }
    }
}
