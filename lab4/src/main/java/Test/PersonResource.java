package Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersonResource {
    
    private Person person;

    @Autowired
    public PersonResource(Person person) {
        this.person = person;
    }

    public Person getPerson(){
        return this.person;
    }

    public void setPerson(Person personInput){
        this.person = personInput;
    }
}
